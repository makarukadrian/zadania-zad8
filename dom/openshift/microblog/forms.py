# -*- coding: utf-8 -*-
from django.forms import ModelForm, HiddenInput, Textarea, PasswordInput
from django import forms
from .models import Post
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'autor', 'tytul', 'tekst', 'data', 'tagi'
        ]
        widgets = {
            'tekst': Textarea(),
            'autor': HiddenInput(),
            'data': HiddenInput(),
        }

    def cleanty(self):
        tytul = self.cleaned_data['tytul']
        if len(tytul) < 4 or len(tytul) > 40:
            raise forms.ValidationError(u"Tytul musi zawierac od 4 do 40 znakow")
        return tytul

    def cleante(self):
        tekst = self.cleaned_data['tekst']
        if len(tekst) < 10 or len(tekst) > 200:
            raise forms.ValidationError(u"Post musi zawierac od 10 do 200 znakow")
        return tekst

class LoginForm(ModelForm):
    class Meta:
        model = User
        fields = [
            'username', 'password'
        ]
        widgets = {
            'password' : PasswordInput(),
        }

class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required = True)

    class Meta:
        model = User
        fields = [
            'username', 'email', 'password1', 'password2'
        ]

        def save(self, commit = True):
            user = super(MyRegistrationForm, self).save(commit = False)
            user.email = self.cleaned_data['email']

            if commit:
                user.save()

            return user

    def cleane(self):
        email = self.cleaned_data['email']
        users = User.objects.all()
        for user in users:
            if email == user.email:
                raise forms.ValidationError(u"Uzyj innego maila")
        return email

    def cleanu(self):
        username = self.cleaned_data['username']
        dlugosc = len(username)
        if dlugosc < 4:
            raise forms.ValidationError(u"Login musi mieć więcej niż 4 znaki")
        return username

    def cleanp(self):
        password1 = self.cleaned_data['password1']
        dlugosc = len(password1)
        if dlugosc < 4:
            raise forms.ValidationError(u"Haslo musi miec wiecej niz 4 znaki")
        return password1