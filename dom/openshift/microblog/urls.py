from django.conf.urls import patterns, url, include
from .views import *

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', index, name='index'),
    url(r'^register$', rejestracja, name='register'),
    url(r'^login$', zaloguj, name='login'),
    url(r'^logout$', wyloguj, name='logout'),
    url(r'^user/(?P<pk>\d+)$', posty, name='user'),
    url(r'^email/(?P<key>.+)$', aktywacja, name='email'),	
    url(r'^dodaj$', dodajPost, name='dodaj'),
    url(r'^postyM$', MojePosty, name='postyM'),
    url(r'^filtr$', filtruj, name='filtr'),
    url(r'^edit/(?P<pk>\d+)$', edycja, name='edit'),
    url(r'^editpost/(?P<pk>\d+)$', edytowanie, name='save'),
    url(r'^search$', przeszukaj, name='search'),
    url(r'^search/(?P<pk>\d+)$', SzukajT, name='tag'),
)