# -*- coding: utf-8 -*-
from django.test import TestCase
from django.contrib.auth.models import User

class MicroblogTest(TestCase):
    
    @staticmethod
    def CreateUser():
        User.objects.create_user(username='asdew', password='asdew', email='asdew@asdew.com')

    def test_200_RegisterP(self):
        response = self.client.get('/register')
        assert response.status_code == 200

    def test_log_and_logout(self):
        self.CreateUser()
        response = self.client.get('/')
        self.assertContains(response, u'Log')
        self.client.login(username='asdew', password='asdew')
        response = self.client.get('/')
        self.assertContains(response, u'Logout')

    def test_200_Login_PageCP(self):
        self.CreateUser()
        self.client.login(username='asdew', password='asdew')
        response = self.client.get('/')
        self.assertContains(response, u'<p><b>Log:</b> asdew</p>')

    def test_200_Login_PageWP(self):
        self.client.login(username='asdew', password='asdew')
        response = self.client.get('/')
        self.assertNotContains(response, u'<p><b>Log:</b> asdew</p>')
		
    def test_200_MainP(self):
        response = self.client.get('/')
        assert response.status_code == 200

    def test_302_AddP(self):
        response = self.client.get('/dodaj')
        assert response.status_code == 302

    def test_users(self):
        self.CreateUser()
        response = self.client.get('/filtr')
        self.assertContains(response, u'<p><a href="/user/2">testos</a></p>')


