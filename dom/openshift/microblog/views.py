# -*- coding: utf-8 -*-

from .models import Post, User, Tag, UserKey
from .forms import PostForm, LoginForm, MyRegistrationForm
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.contrib.auth.decorators import login_required,
from django.core.mail import send_mail
from uuid import uuid4 as uuid
from django.shortcuts import get_object_or_404


def Tabela(request, posty):
    tabela = []
    for post in posty:
        if (timezone.now() - post.data).seconds / 60 < 10:
            if post.autor == request.user:
                tabela.append(post.pk)
        else:
            break
    return tabela


def index(request):
    info = request.session.get('info', None)
    if info:
        del request.session['info']
    posty = Post.objects.all().order_by('-pk')
    tabela = Tabela(request, posty)
    return render(request, 'microblog/main.html', {"posty" : posty, "info" : info, "tabela" : tabela})

def rejestracja(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            WyslijEmail(request.POST['username'])
            request.session['info'] = u'Zarejestrowales sie, potwierdz utworzenie konta przez email.'
            return HttpResponseRedirect(reverse('index'))
    else:
        form = MyRegistrationForm()
    return render(request, "microblog/rejestracja.html", {'form': form})

def zaloguj(request):
    info = request.session.get('info', None)
    if request.method == 'GET':
        if request.user.is_authenticated():
            request.session['info'] = u'Jestes zalogowany'
            return HttpResponseRedirect(reverse('index'))
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'microblog/logowanie.html', {"info" : info, 'form' : form})
    elif request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        try:
            is_activte = UserKey.objects.get(user=user).is_active
        except:
            is_activte = True
        if user and is_activte:
            login(request, user)
            request.session['info'] = u'Zalogowales sie'
            return HttpResponseRedirect(reverse('index'))
        elif is_activte:
            info = u'Blad w danych'
        else:
            info = u'Musisz potwierdzic utworzenie konta'
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'microblog/logowanie.html', {"info" : info, 'form' : form})

def wyloguj(request):
    logout(request)
    request.session['info'] = u'Wylogowales sie'
    return HttpResponseRedirect(reverse('index'))

def dodajPost(request):
    if request.method == 'GET':
        if request.user.is_authenticated():
            form = PostForm(initial={'autor': request.user, 'data': timezone.now(), 'dateE': None})
            return render(request, 'microblog/dodaj.html', {"user_nick" : request.user.username, "form" : form})
        request.session['info'] = u'Nie jestes zalogowany'
        return HttpResponseRedirect(reverse('login'))
    elif request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save()
            request.session['info'] = u'Post zostal dodany'
            return HttpResponseRedirect(reverse('index'))
        return render(request, 'microblog/dodaj.html', {"form" : form})

def posty(request, pk):
    try:
        user = User.objects.get(pk=pk)
    except:
        return render(request, 'microblog/postyU.html', {"posty" : None, 'postU' : None})
    posty = Post.objects.all().filter(autor_id=pk).order_by('-pk')
    return render(request, 'microblog/postyU.html', {"posty" : posty, 'postU' : user})

def MojePosty(request):
    if request.user.is_authenticated():
        autor = User.objects.get(username=request.user.username)
        posty = Post.objects.all().order_by('-pk').filter(autor=autor)
        tabela = Tabela(request, posty)
        return render(request, 'microblog/postyM.html', {"posty" : posty, "tabela": tabela})
    else:
        request.session['info'] = u'Nie jestes zalogowany'
        return HttpResponseRedirect(reverse('login'))




def filtruj(request):
    users = User.objects.all().order_by('username')
    return render(request, 'microblog/uzytkownicy.html', {"users" : users})


def aktywacja(request, key):
    uk = UserKey.objects.get(key=key)
    uk.is_active = True
    uk.save()
    request.session['info'] = u'Twoje konto zostalo aktywowane.'
    return HttpResponseRedirect(reverse('index'))

def WyslijEmail(username):
    user = User.objects.get(username=username)
    key = uuid().hex
    uk = UserKey(key=key, user=user, is_active=False)
    uk.save()
    od = 'zad8@microblog.pl'
    do = user.email
    subject = 'Aktywacja konta'
    message = 'Aby aktywowac konto prosimy o klikniecie w ponizszy link:\n' \
    'Link : ' + 'http://mysite-zadpwi.rhcloud.com/blog/email/' + key
    send_mail(subject, message, od, [do])

def Moderator(request):
    perms = request.user.groups.all()
    for perm in perms:
        if perm.name == 'Moderator':
            return True
    return request.user.has_perm('microblog.change_post')
@login_required(login_url='/login')
def edycja(request, pk):
    try:
        post = Post.objects.get(pk=pk)
        if ((timezone.now() - post.data).seconds / 60 < 10 and post.autor == request.user) or Moderator(request):
            form = PostForm(initial={'autor': request.user, 'data': post.data, 'tytul': post.tytul, 'tekst':post.tekst, 'dateE': None})
            return render(request, 'microblog/edytuj.html', {'form': form, 'id':post.pk})
        request.session['info'] = u'Operacja nie mozliwa'
        return HttpResponseRedirect(reverse('index'))
    except:
        request.session['info'] = u'Brak wpisu'
        return HttpResponseRedirect(reverse('index'))

@login_required(login_url='/login')
def edytowanie(request, pk):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            edytuj = form.save(commit=False)
            post = Post.objects.get(pk=pk)
            alert = False
            if len(post.tagi.all()) != len(form.cleaned_data['tagi']):
                alert = True
            else:
                for i in range(0, len(post.tagi.all())):
                    if post.tagi.all()[i].tekst != form.cleaned_data['tagi'][i].tekst:
                        alert = True
                        break
            if edytuj.tytul != post.tytul or edytuj.tekst != post.tekst or alert:
                post.dateE = timezone.now()
                post.edytor = User.objects.get(pk=request.user.id)
                post.tytul = edytuj.tytul
                post.tekst = edytuj.tekst
                post.tagi.clear()
                try:
                    for tekst in form.cleaned_data['tagi']:
                        tag = Tag.objects.get(tekst=tekst)
                        post.tagi.add(tag)
                except:
                    pass
                post.save()

                request.session['info'] = u'Post zostal zmieniony'
                return HttpResponseRedirect(reverse('index'))
        else:
            return render(request, 'microblog/edytuj.html', {"form" : form, "id": pk})
    return HttpResponseRedirect(reverse('index'))


def przeszukaj(request):
    tagi = Tag.objects.all()
    return render(request, 'microblog/tagi.html', {"tagi" : tagi})

def SzukajT(request, pk):
    #tag = get_object_or_404(Tag, pk=int(pk))
    #posty = tag.post_set.all().order_by("-data")
    posty = Post.objects.all().order_by('-pk').filter(tagi__in=pk)
    tabela = Tabela(request, posty)
    return render(request, 'microblog/sTag.html', {"posty" : posty, "tabela": tabela})
