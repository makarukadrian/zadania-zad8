# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class Tag(models.Model):
    tekst = models.CharField(max_length=30)

    def __unicode__(self):
        return self.tekst

class Post(models.Model):
    autor = models.ForeignKey(User, related_name='autor', unique=False)
    data = models.DateTimeField(u'Data postu')
    tytul = models.CharField(max_length=40)
    tekst = models.TextField(u'Tekst postu')
    tagi = models.ManyToManyField(Tag, blank=True, null=True)
    edytor = models.ForeignKey(User, null=True, related_name='edytor', unique=False)
    dateE = models.DateTimeField(u'Data edycji', null=True)




class UserKey(models.Model):
    key = models.CharField(max_length=50)
    user = models.OneToOneField(User)
    is_active = models.BooleanField()